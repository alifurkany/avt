# avt: ali's video tool

    avt: ali's video tool
    Usage: ./avt rec|record
           ./avt comp|compress
           ./avt nvid|normalize
           ./avt help
    Environment variables:
    avt_debug: print every command before running [set to enable]

    Commands:

    ./avt rec / record
    Losslessly record screen and audio
    Usage: ./avt rec
    Enviroment variables:
    rec_display: set x11 display (default: $DISPLAY)
    rec_out: set output file (default: ~/Desktop/$(date +%F_%H-%M-%S).mkv)
    rec_fps: set framerate (default: 60)
    rec_res: set resolution (default: rec_display or $DISPLAY's resolution) (automatically clamped)
    rec_preset: set preset (default: ultrafast)
    rec_vrate: replace -crf options
    rec_crf: set crf, use 0 for lossless (default: 0)
    rec_adev: space separated pulseaudio source devices (default: default and first running monitor)
    rec_ar: set audio samplerate (autodetect default)
    rec_compress: immediately start comp after recording ends [set to enable]

    ./avt comp / compress
    Compress a recording without minimal quality loss
    Usage: ./avt comp <file> [outfile]
    Environment variables:
    comp_crf: set crf, this should be greater than or equals to rec_crf, use 0 for lossless (default: 10)
    comp_ac: set audio codec (default: libopus)
    comp_ba: set audio bitrate (default: 320k)
    comp_ar: set audio samplerate, this should be less than or equals to rec_ar (default: 48000)

    ./avt nvid / normalize
    Normalize video & audio for backwards compatibility and lower file size
    Usage: ./avt nvid <file> [outfile]
    Environment variables:
    nvid_preset: set preset (default: veryslow)
    nvid_vrate: replace -crf options
    nvid_crf: set crf value (default: 19)
    nvid_rgb: use libx264rgb [set to enable]
    nvid_pixfmt: set pixel format (default: yuv420p)
    nvid_profile: set profile (default: high)
    nvid_ac: set audio codec (default: libopus)
    nvid_ba: set audio bitrate (default: 160k)
    nvid_ar: set audio samplerate (default: 48000)

    avt will try to source ~/.avtrc if it exists. You have access to the findmonitor function in there.

# Dependencies

-   `bash`
-   `ffmpeg`
-   `pipewire`
-   `pulseaudio`

# Requirements

This assumes you use X11, PipeWire and pipewire-pulse.  
You also need a pretty fast disk and a lot of free space to record with this tool with the default settings. Consider increasing [`rec_crf`](https://trac.ffmpeg.org/wiki/Encode/H.264#crf) or choosing a slower [`rec_preset`](https://trac.ffmpeg.org/wiki/Encode/H.264#Preset) if you don't have those.

# Q/A

-   Q: How do I record a region of the screen?
    A: `rec_display=$DISPLAY+<start x>,<start y> rec_res=<width>x<height>`
